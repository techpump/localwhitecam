## Introducción

El principal objetivo del proyecto es la instalación y ejecución del proyecto WhiteCam de la forma más sencilla posible.

Prueba de selección de Techpump

## Requerimientos

* Tener instalada la versión 2.2.9 de Vagrant para evitar problemas de checksum
* Tener instalado Virtualbox (el proyecto se ha probado en VirtualBox 5).

La primera instalación tarda debido a que descarga la imagen de Vagrant por primera vez

## Instalación

Lanzar el siguiente comando para clonar el proyecto de entorno local en el directorio que queramos:
`git clone git@gitlab.com:techpump/localwhitecam.git`

Una vez clonado, entrar al directorio del proyecto clonado y lanzar el comando `make install` . Tardará unos minutos ya que tiene que descargar la imagen de Vagrant

Cuando haya finalizado, tendremos ya nuestro entorno local listo para trabajar. Para parar dicho entorno, podemos lanzar el comando `make stop` y la virtualización se parará.

## Makefile

Dentro del proyecto hay un makefile a través del que podemos ejecutar diversos comandos:

###### help

Lanza una pequeña ayuda sobre los comandos make disponibles

###### install

Es el principal comando del proyecto y sirve para realizar una instalación desde cero del proyecto WhiteCam. Elimina el proyecto si existía y levanta un entorno de desarrollo basado en Homestead con todas las herramientas necesarias.

###### run

Arranca la máquina de vagrant con los archivos necesarios

###### run-provision

Arranca la máquina de vagrant con los archivos necesarios con el parámetro de aprovisionamiento de la máquina --provision

###### stop

Para la máquina de Vagrant

###### phpunit

Lanza dentro del proyecto los test unitarios

###### phpunit-coverage

Lanza dentro del proyecto los test unitarios y genera un informe con el coverage del proyecto

###### composer-install

Lanza dentro del proyecto el comando composer install.

###### composer-update

Lanza dentro del proyecto el comando composer update.

###### ssh

Entra dentro de la máquina virtual de Vagrant



