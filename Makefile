DIR_PROJECT=~/techPumpAFP/whiteCam
DIR_GITLAB=git@gitlab.com:techpump/whitecam.git
DIR_WHITECAM_FILES=whiteCamFiles/
DIR_ENV_LOCAL=$(DIR_WHITECAM_FILES).env.local
DIR_ENV_TEST=$(DIR_WHITECAM_FILES)/.env.test.local

error:
	@echo "Please choose a target (install, run, stop, unit-tests...) or type 'make help' for more info"

install:
	@if [ -d $(DIR_PROJECT) ]; then rm -Rf $(DIR_PROJECT); fi
	@mkdir -p $(DIR_PROJECT)
	@git clone $(DIR_GITLAB) $(DIR_PROJECT)
	@chmod -R 777 $(DIR_WHITECAM_FILES)
	@cp $(DIR_ENV_LOCAL) $(DIR_PROJECT) && cp $(DIR_ENV_TEST) $(DIR_PROJECT)
	@if grep "whitecam.test" /etc/hosts; then echo 'Ya existe whitecam.test en /etc/hosts'; else echo '192.168.10.10 whitecam.test' | sudo tee -a /etc/hosts; fi
	@if grep "babosas.biz" /etc/hosts; then echo 'Ya existe babosas.biz en /etc/hosts'; else echo '192.168.10.10 www.babosas.biz' | sudo tee -a /etc/hosts; fi
	@if grep "cerdas.com" /etc/hosts; then echo 'Ya existe cerdas.com en /etc/hosts'; else echo '192.168.10.10 www.cerdas.com' | sudo tee -a /etc/hosts; fi
	@if grep "conejox.com" /etc/hosts; then echo 'Ya existe conejox.com en /etc/hosts'; else echo '192.168.10.10 www.conejox.com' | sudo tee -a /etc/hosts; fi
	@vagrant destroy -f
	@vagrant up --provision
	@vagrant ssh --command "cd $(DIR_PROJECT) && composer install"

run:
	@vagrant up

run-provision:
	@vagrant up --provision

stop:
	@vagrant halt

phpunit:
	@vagrant ssh --command "cd $(DIR_PROJECT) && vendor/bin/phpunit"

phpunit-coverage:
	@vagrant ssh --command "cd $(DIR_PROJECT) && vendor/bin/phpunit --coverage-html=public/coverage"

composer-install:
	@vagrant ssh --command "cd $(DIR_PROJECT) && composer install"

composer-update:
	@vagrant ssh --command "cd $(DIR_PROJECT) && composer update"

ssh:
	@vagrant ssh

help:
	@echo "make install             - installs the local environment"
	@echo "make run                 - starts the local environment"
	@echo "make run-provision       - starts the local environment with --provision"
	@echo "make stop                - stops the local environment"
	@echo "make phpunit             - runs the phpunit test"
	@echo "make phpunit-coverage    - runs the phpunit test with coverage"
	@echo "make composer-install    - runs composer install into whiteCam"
	@echo "make composer-update     - runs composer update into whiteCam"
	@echo "make ssh                 - enter into virtual machine"

.PHONY: error install run run-provision stop phpunit composer-install composer-update